package drawer_test

import (
	"image/color"
	"testing"

	"codeberg.org/maxtar/graphlearning/drawer"
)

func BenchmarkLine(b *testing.B) {
	d := drawer.NewDrawer(4000, 4000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.OldLine(10, 10, 600, 996, color.White)
	}
}

func BenchmarkLineNewF(b *testing.B) {
	d := drawer.NewDrawer(4000, 4000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.LineNewF(10, 10, 600, 996, color.White)
	}
}

func BenchmarkLineNewI(b *testing.B) {
	d := drawer.NewDrawer(4000, 4000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Line(10, 10, 600, 996, color.White)
	}
}

func BenchmarkFlipV(b *testing.B) {
	d := drawer.NewDrawer(4000, 4000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.FlipV()
	}
}

func BenchmarkFlipVOld(b *testing.B) {
	d := drawer.NewDrawer(4000, 4000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.FlipVOld()
	}
}
