package drawer

import (
	"image"
	"image/color"
	"image/draw"
	"math"
)

type Drawer struct {
	img *image.NRGBA
}

// NewDrawer create new instance of Drawer and initializes it.
func NewDrawer(width, height int) *Drawer {
	img := image.NewNRGBA(image.Rect(0, 0, width, height))
	draw.Draw(img, img.Bounds(), image.Black, image.Point{}, draw.Src)
	return &Drawer{img: img}
}

// Img return current  image as image.NRGBA type.
func (d *Drawer) Img() *image.NRGBA {
	return d.img
}

// OldLineF draws line with Bresenham's line algorithm
func (d *Drawer) OldLineF(x0, y0, x1, y1 int, color color.Color) {
	for t := 0.0; t < 1.0; t += .01 {
		x := float64(x0)*(1.0-t) + float64(x1)*t
		y := float64(y0)*(1.0-t) + float64(y1)*t
		d.img.Set(int(x), int(y), color)
	}
}

// OldLineI draws line uses int type instead of float from Bresenham's line algorithm
func (d *Drawer) OldLineI(x0, y0, x1, y1 int, color color.Color) {
	for x := x0; x <= x1; x++ {
		t := float64(x-x0) / float64(x1-x0)
		y := float64(y0)*(1.0-t) + float64(y1)*t
		d.img.Set(x, int(y), color)
	}
}

// OldLine realized for any directions
func (d *Drawer) OldLine(x0, y0, x1, y1 int, color color.Color) {
	steep := false
	if absI(x0-x1) < absI(y0-y1) {
		x0, y0 = y0, x0
		x1, y1 = y1, x0
		steep = true
	}
	if x0 > x1 {
		x0, x1 = x1, x0
		y0, y1 = y1, y0
	}

	for x := x0; x <= x1; x++ {
		t := float64(x-x0) / float64(x1-x0)
		y := float64(y0)*(1.0-t) + float64(y1)*t
		if steep {
			d.img.Set(int(y), x, color)
		} else {
			d.img.Set(x, int(y), color)
		}
	}
}

func (d *Drawer) LineNewF(x0, y0, x1, y1 int, color color.Color) {
	steep := false
	if absI(x0-x1) < absI(y0-y1) {
		x0, y0 = y0, x0
		x1, y1 = y1, x0
		steep = true
	}
	if x0 > x1 {
		x0, x1 = x1, x0
		y0, y1 = y1, y0
	}

	dx := float64(x1 - x0)
	dy := float64(y1 - y0)
	dError := math.Abs(dy / dx)
	e := 0.0
	y := y0
	for x := x0; x <= x1; x++ {
		if steep {
			d.img.Set(y, x, color)
		} else {
			d.img.Set(x, y, color)
		}
		e += dError

		if e > 0.5 {
			if y1 > y0 {
				y += 1
			} else {
				y += -1
			}
			e -= 1.0
		}
	}
}

func (d *Drawer) Line(x0, y0, x1, y1 int, color color.Color) {
	steep := false
	if absI(x0-x1) < absI(y0-y1) {
		x0, y0 = y0, x0
		x1, y1 = y1, x0
		steep = true
	}
	if x0 > x1 {
		x0, x1 = x1, x0
		y0, y1 = y1, y0
	}

	dx := x1 - x0
	dy := y1 - y0
	dError := absI(dy) * 2
	e := 0
	y := y0
	for x := x0; x <= x1; x++ {
		if steep {
			d.img.Set(y, x, color)
		} else {
			d.img.Set(x, y, color)
		}

		e += dError

		if e > dx {
			if y1 > y0 {
				y += 1
			} else {
				y += -1
			}
			e -= dx * 2
		}
	}
}

func absI(n int) int {
	if n < 0 {
		return 0 - n
	}
	return n
}

func (d *Drawer) FlipVOld() {
	bounds := d.img.Bounds()
	dst := image.NewNRGBA(bounds)
	dy := bounds.Max.Y - 1
	// Move to right by X
	for x := bounds.Min.X; x < bounds.Max.X; x++ {
		// Move by Y
		for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
			dst.Set(x, y, d.img.At(x, dy-y))
		}
	}
	d.img = dst
}

// Flip image by vertical
func (d *Drawer) FlipV() {
	bounds := d.img.Bounds()
	dy := bounds.Max.Y - 1
	// Move by Y
	for y := bounds.Min.Y; y < bounds.Max.Y/2; y++ { //todo need to parallel
		// Move to right by X
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			src := d.img.At(x, y)
			dst := d.img.At(x, dy-y)
			d.img.Set(x, y, dst)
			d.img.Set(x, dy-y, src)
		}
	}
}
