[![Go Reference](https://pkg.go.dev/badge/codeberg.org/maxtar/graphlearning.svg)](https://pkg.go.dev/codeberg.org/maxtar/graphlearning)

# graphlearning

Тренировка работы с графикой.

Основано на серии статей на [Хабре](https://habr.com/ru/post/248153/).
Репозиторий на [GitHub](https://github.com/ssloy/tinyrenderer/tree/f6fecb7ad493264ecd15e230411bfb1cca539a12).

Ещё одна серия статей:

1. [Трёхмерная графика с нуля. Часть 1: трассировка лучей](https://habr.com/ru/post/342510/).
2. [Трёхмерная графика с нуля. Часть 2: растеризация](https://habr.com/ru/post/342708/)
