package main

import (
	"image"
	"image/color"
	"image/png"
	"log"
	"os"

	"codeberg.org/maxtar/graphlearning/drawer"
)

const (
	width  = 100
	height = 100
)

var (
	red   = color.NRGBA{255, 0, 0, 255}
	green = color.NRGBA{0, 255, 0, 255}
	blue  = color.NRGBA{0, 0, 255, 255}
	white = image.White
)

func main() {
	d := drawer.NewDrawer(width, height)
	d.OldLine(13, 20, 80, 40, red)
	// d.LineI(13, 20, 80, 40, image.White)
	// d.Line(10, 80, 20, 10, red)
	// d.LineI(10, 80, 20, 10, image.White)

	d.Line(13, 20, 80, 40, white)
	d.OldLineI(20, 13, 40, 80, red)
	d.LineNewF(80, 40, 13, 20, green)

	d.FlipV()
	f, err := os.Create("test.png")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	if err = png.Encode(f, d.Img()); err != nil {
		log.Fatal(err)
	}
}
